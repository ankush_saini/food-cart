# Food Cart Assignment

This is the base folder for the food cart exercise. You will have all the static assets and mock server responses available in this repo.

## Getting Started

Clone this repo and run following command for starting the mock server:

```
npm install
npm run dev
# or
yarn install
yarn dev
```
