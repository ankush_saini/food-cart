import {
  ADD_TO_CART,
  OPEN_CART_OVERLAY,
  CLOSE_CART_OVERLAY,
  CART_ITEMS,
} from "../constant/action";

const Reducer = (state, action) => {
  switch (action.type) {
    case ADD_TO_CART:
      return {
        ...state,
        ...action.payload,
      };

    case OPEN_CART_OVERLAY:
      return {
        ...state,
        ...action.payload,
      };

    case CLOSE_CART_OVERLAY:
      return {
        ...state,
        ...action.payload,
      };

    case CART_ITEMS:
      return {
        ...state,
        ...action.payload,
      };
  }
};

export default Reducer;
