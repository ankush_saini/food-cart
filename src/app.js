import "react-app-polyfill/ie11";
import "react-app-polyfill/stable";
import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter as Router } from 'react-router-dom';

import Store from "./store";

import Main from "./main";
import ErrorBoundary from './errorBoundary';
import Header from "./components/organisms/header";
import Footer from "./components/organisms/footer";

import "./index.scss";

const App = () => {
  return (
    <Router>
      <Header />
      <Main />
      <Footer />
    </Router>
  );
};

ReactDOM.render(
  <ErrorBoundary>
    <Store>
      <App />
    </Store>
  </ErrorBoundary>,
  document.getElementById("root")
);
