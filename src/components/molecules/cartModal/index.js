import React from "react";
import Modal from "../../atoms/modal";
import Cart from "../../page/cartPage";
import "./index.scss";

const CartModal = (props) => {
  return (
    <Modal showModal={props.showModal}>
      <Cart handleClose={props.handleClose} />
    </Modal>
  );
};

export default CartModal;
