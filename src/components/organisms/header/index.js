import React, { useContext } from "react";
import { Link, useNavigate } from "react-router-dom";
import HeaderLogo from "../../atoms/headerLogo";
import { Context } from "../../../store";
import CartModal from "../../molecules/cartModal";

import { useDevice } from "../../../utils";
import {
  OPEN_CART_OVERLAY,
  CLOSE_CART_OVERLAY,
} from "../../../constant/action";

import "./index.scss";

import cartIcon from "../../../../static/images/cart.svg";

import {
  homeCtaLabel,
  productCtaLabel,
  itemLabel,
} from "../../../constant";

const Header = () => {
  const navigate = useNavigate();
  const [state, dispatch] = useContext(Context);
  const { isDesktop } = useDevice();

  const handleCloseCart = () => {
    isDesktop &&
      dispatch({
        type: CLOSE_CART_OVERLAY,
        payload: { showPopup: false },
      });
  };

  const handleCartModal = () => {
    isDesktop &&
      dispatch({
        type: OPEN_CART_OVERLAY,
        payload: { showPopup: true },
      });
  };

  const handleCartClick = () => {
    if (!isDesktop) {
      navigate("/cart");
    } else {
      handleCartModal();
    }
  };
  return (
    <>
      <header className="header">
        <div className="container-fluid">
          <div className="logo_container_nav">
            <HeaderLogo />
            <div className="nav_links">
              <Link to="/home">{homeCtaLabel}</Link>
              <Link to="/products">{productCtaLabel}</Link>
            </div>
          </div>
          <div className="nav_container"> 
            <div className="nav_container__item">
              <span onClick={handleCartClick}>
                <div className="cart_logo_container">
                  <img className="cart_logo" src={cartIcon} alt="cart-logo" />
                  <span>{`${state.itemCount} ${itemLabel}`}</span>
                </div>
              </span>
            </div>
          </div>
        </div>
      </header>
      {state.showPopup && isDesktop && (
        <CartModal showModal={state.showPopup} handleClose={handleCloseCart} />
      )}
    </>
  );
};

export default Header;
