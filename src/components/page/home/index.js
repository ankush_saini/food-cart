import React, { useEffect, useState } from "react";

import Loader from "../../../components/atoms/loader";
import Carousel from "../../../components/molecules/carousel";
import Categories from "../../../components/molecules/categories";

import "./index.scss";

import { FetchData } from "../../../utils";
import { getBannerApi } from "../../../services";

const Home = () => {
  const [offers, setOffers] = useState([]);

  const getOffers = () => {
    FetchData(getBannerApi)
      .then((res) => {
        setOffers(res);
      })
      .catch((err) => {
        console.log("ERROR detected fetching products", err);
      });
  };

  useEffect(() => {
    getOffers();
  }, []);

  if(!offers.length) {
    return <Loader show={true} />
  }

  return (
    <div className="container-fluid wrapper">
      <Carousel offers={offers} />
      <Categories />
    </div>
  );
};

export default Home;
