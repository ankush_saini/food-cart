import React, { lazy, Suspense } from "react";
import { Routes, Route } from 'react-router-dom';

import { useDevice } from "../utils";
import Loader from "../components/atoms/loader";

const Home = lazy(() => import("../components/page/home"));
const ProductList = lazy(() => import("../components/page/productList"));
const Cart = lazy(() => import("../components/page/cartPage"));
const NotFound = lazy(() => import("../components/page/404"));

const Spinner = () => {
  return (
    <div className="throbberOverlay">
      <Loader show={true} />
    </div>
  );
};

const Main = () => {
  const { isDesktop } = useDevice();
  return (
    <>
      <Suspense fallback={<Spinner />}>
        <Routes>
          <Route path="/">
            <Route index element={<Home />} />
            <Route path="/home" exact element={<Home />} />
            <Route path="/products" element={<ProductList />} />
            {!isDesktop && <Route path="/cart" element={<Cart />} />}
          </Route>
          <Route path="*" element={<NotFound />} />
        </Routes>
      </Suspense>
    </>
  );
};

export default Main;
