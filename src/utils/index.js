import FetchData from "./fetch-data";
import useDevice from "./customHooks/useDevices";

export { FetchData, useDevice };
