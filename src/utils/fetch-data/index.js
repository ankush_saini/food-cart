"use strict";

const defaults = {
  mode: "cors",
  cache: "default",
  method: "GET",
  credentials: "same-origin",
};

// * Export a standard `fetch` method containing necessary global options
// *
// * @module FetchData
// * @param {String} url Path to supply to `fetch` method
// * @param {String} [opts] Optional parameters to supply or override default options. Default value is {}

export default function (url, opts = {}) {
  const options = { ...defaults, ...opts };
  return fetch(url, options)
    .then((response) => {
      const responseStatus = response.status;
      return response
        .json()
        .then((data) => {
          return data;
        })
        .catch((e) => {
          const err = {
            status: responseStatus,
            error: e,
          };
          return err;
        });
    })
    .catch((error) => {
      console.error(error)
    } /* console.error(error)*/);
}
