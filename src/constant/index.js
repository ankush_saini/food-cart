// common label
export const selectCategoryLabel = "Select Category";
export const addToCartLabel = "BUY NOW";
export const mrpLabel = "MRP";
export const ruppeeLabel = "Rs.";
export const newPriceLabel = "₹";
export const homeCtaLabel = "Home";
export const productCtaLabel = "Products";
export const myAccountLabel = "My Account";
export const itemLabel = "Items";

// 404 error page label
export const errorTitleLabel = "OOPS!";
export const errorDescriptionLabel = "404- THE PAGE CAN'T BE FOUND.";
export const goToHomeCtaLabel = "GO TO HOMEPAGE";

// Cart Page label
export const cartTitleLabel = "MY CART";
export const cartItemEmptyLabel = "No items in your cart.";
export const favoriteItemLabel = "Your favorite items are just a click away.";
export const bestBuyLabel = "You won't find it cheaper anywhere.";
export const promoCodeApplyLabel = "Promo can be applied on payment page.";
export const proceedToCheckoutLabel = "Proceed to Checkout";
export const totalAmountLabel = "Total Amount";
export const continueShoppingLabel = "Start Shopping";