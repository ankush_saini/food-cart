/**
 * ACTION 
 *
 * Add actions to be used here
 */
"use strict";

export const ADD_TO_CART = "addedToCartProduct";
export const ORDER_ID = "orderId";
export const OPEN_CART_OVERLAY = "openCartOverlay";
export const CLOSE_CART_OVERLAY = "closeCartOverlay";
export const CART_ITEMS = "cartItems";
