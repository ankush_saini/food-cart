const baseUrl = "https://sabka-bazar-server.netlify.app/.netlify/functions/api";

export const getCategoryApi = `${baseUrl}/categories/`;
export const getProductApi = `${baseUrl}/products/`;
export const getBannerApi = `${baseUrl}/banners/`;
export const getCartApi = "";
export const postCartApi = "";
